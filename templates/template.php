<!DOCTYPE html>
<html>
<head>
	<title>My ecommerce store</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/bootstrap.css">
</head>
<body>
	<?php require 'navbar.php'?>
	<?php get_content() ?>
	<?php require 'footer.php'?>
</body>
</html>

